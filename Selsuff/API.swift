// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class UserLoginMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation UserLogin($email: String!, $password: String!) {
      userLogin(input: {email: $email, password: $password}) {
        __typename
        status
        firstName
        accessToken
      }
    }
    """

  public let operationName: String = "UserLogin"

  public var email: String
  public var password: String

  public init(email: String, password: String) {
    self.email = email
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["email": email, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("userLogin", arguments: ["input": ["email": GraphQLVariable("email"), "password": GraphQLVariable("password")]], type: .nonNull(.object(UserLogin.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(userLogin: UserLogin) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "userLogin": userLogin.resultMap])
    }

    public var userLogin: UserLogin {
      get {
        return UserLogin(unsafeResultMap: resultMap["userLogin"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "userLogin")
      }
    }

    public struct UserLogin: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["LoginUser"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("status", type: .nonNull(.scalar(Bool.self))),
        GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(status: Bool, firstName: String, accessToken: String) {
        self.init(unsafeResultMap: ["__typename": "LoginUser", "status": status, "firstName": firstName, "accessToken": accessToken])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var status: Bool {
        get {
          return resultMap["status"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var firstName: String {
        get {
          return resultMap["firstName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstName")
        }
      }

      public var accessToken: String {
        get {
          return resultMap["accessToken"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "accessToken")
        }
      }
    }
  }
}
