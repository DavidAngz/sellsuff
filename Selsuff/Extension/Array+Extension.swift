//
//  Array+Extension.swift
//  Attendance
//
//  Created by mike lim on 19/02/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension Array {
    public func get(_ index: Int) -> Element? {
        guard index >= 0 && index < self.count else {
            return nil
        }
        return self[index]
    }
}

extension Array where Element: UIView {
    func style(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
}

extension Array where Element: UIBarItem {
    func style(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
}

extension Array where Element: UIGestureRecognizer {
    func style(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
