//
//  UIApplication+Extension.swift
//  Attendance
//
//  Created by mike lim on 08/04/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
    static func getTopViewController() -> UIViewController {
        
        var viewController = UIViewController()
        
        if let vc =  self.shared.delegate?.window??.rootViewController {
            
            viewController = vc
            var presented = vc
            
            while let top = presented.presentedViewController {
                presented = top
                viewController = top
            }
        }
        
        return viewController
    }
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
          if let nav = base as? UINavigationController {
              return topViewController(base: nav.visibleViewController)
          }
          if let tab = base as? UITabBarController {
              if let selected = tab.selectedViewController {
                  return topViewController(base: selected)
              }
          }
          if let presented = base?.presentedViewController {
              return topViewController(base: presented)
          }
          return base
      }
    
}
