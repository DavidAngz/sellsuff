//
//  UIAppearance.swift
//  DextionAttendance
//
//  Created by mike lim on 23/01/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension UIAppearance {
    
    func style(_ completion: @escaping ((Self)->Void)) {
        completion(self)
    }
    
}
