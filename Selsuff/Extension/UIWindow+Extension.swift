//
//  UIWindow+Extension.swift
//  Attendance
//
//  Created by mike lim on 25/01/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
    
    public func current() -> UIWindow? {
        return AppDelegate.shared.window
    }
    
}
