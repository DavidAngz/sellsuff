//
//  UIImage+Extension.swift
//  Attendance
//
//  Created by mike lim on 01/07/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension UIImage {
    
    public func resize(_ size:CGSize) -> UIImage {
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage ?? self
    }
    
    public var transparent: UIImage {
        return self.withRenderingMode(.alwaysTemplate)
    }
    
}

