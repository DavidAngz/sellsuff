//
//  BaseModel.swift
//  Attendance
//
//  Created by mike lim on 30/01/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol BaseModelProtocol {
    init(_ json : JSON)
}

protocol BaseMenuProtocol: AnyObject {
    var icon : UIImage? {get set}
    var title : String {get set}
}

class BaseModel<T : BaseModelProtocol> : BaseModelProtocol {
    
    var status: Bool = true
    var messages: [String] = []
    var total: Int = 0
    var data: T?
    var dataArray: [T] = []
    
    required init(_ json: JSON) {
        
        status = json["status"].bool ?? false
        
        if json["data"].dictionary != nil {
            self.data = T(json["data"])
        }
        
        if let data = json["data"].array {
            for item in data {
                dataArray.append(T(item))
            }
        }
        
        if json["messages"].array != nil {
            for message in json["messages"].arrayValue {
                messages.append(message.string ?? "")
            }
        }
        
        if json["total"].int != nil {
            self.total = json["total"].intValue
        }
    }
    
}

